package com.unfpa.mhealth.api_controller.Listeners

import com.unfpa.mhealth.models.KnowledgeablePerson

/**
 * Created by KhyatiShah on 12/9/2019.
 */
interface OnKnowledgeablePersonFetched {
   fun onComplete(lstKnowledgeablePerson: ArrayList<KnowledgeablePerson>)
}