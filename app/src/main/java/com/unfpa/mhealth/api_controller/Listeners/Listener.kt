package com.unfpa.mhealth.api_controller.Listeners

/**
 * Created by KhyatiShah on 10/10/2019.
 */
interface Listener {
    fun onComplete(response:String)
}