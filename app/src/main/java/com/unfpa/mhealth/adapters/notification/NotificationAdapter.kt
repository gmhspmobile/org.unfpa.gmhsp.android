package com.unfpa.mhealth.adapters.notification

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.text.HtmlCompat
import androidx.core.view.MotionEventCompat
import com.unfpa.mhealth.R
import com.unfpa.mhealth.database.MhealthRoomDatabase
import com.unfpa.mhealth.database.entity.my_knowledge.SRHContent
import com.unfpa.mhealth.database.entity.notification.Notification
import com.unfpa.mhealth.fragments.my_knowledge.SRHContentDetailFragment
import com.unfpa.mhealth.fragments.notification.NotificationFragment
import com.unfpa.mhealth.utils.AppUtils
import com.unfpa.mhealth.utils.Constant
import kotlinx.android.synthetic.main.item_notification.view.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

/**
 * Created by KhyatiShah on 12/13/2019.
 */
class NotificationAdapter(notificationFragment: NotificationFragment) :
    androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    val logTag = "ActivitySwipeDetector"
    val MIN_DISTANCE = 100
    private var downX: Float = 0.toFloat()
    private var downY: Float = 0.toFloat()
    private var upX: Float = 0.toFloat()
    private var upY: Float = 0.toFloat()
    private var notificationList = ArrayList<Notification>()
    private var activity: androidx.fragment.app.FragmentActivity = notificationFragment.activity!!
    val notificationFragment = notificationFragment

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): androidx.recyclerview.widget.RecyclerView.ViewHolder {


        return ContentListViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_notification,
                parent,
                false
            )

        )
    }

    override fun getItemCount(): Int = notificationList.size

    override fun onBindViewHolder(viewHolder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        val contentViewHolder = viewHolder as ContentListViewHolder
        contentViewHolder.bindView(notificationList.get(position), position)
    }

    inner class ContentListViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        fun bindView(notification: Notification, position: Int) {
            if (notification.type.equals(Constant.NOTIFICATION_TYPE_MEDICINE)) {
                itemView.imgNotification.setImageResource(R.drawable.ic_noti_medication)
                //itemView.txtHeader.setText(R.string.MedicineReminder)
            } else {
                itemView.imgNotification.setImageResource(R.drawable.ic_noti_appointment)
                //itemView.txtHeader.setText(R.string.AppointmentReminder)
            }
            itemView.txtHeader.text = notification.title
            itemView.txtShortDesc1.text = notification.subTitile1
            itemView.txtShortDesc2.text = notification.subTitile2
            val formatter = SimpleDateFormat(Constant.MEDICINE_TIME_FORMAT_TARGET)
            //formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
            itemView.txtTime.text = formatter.format(Date(notification.notificationTime.toLong()))

            itemView.rlDel.setOnClickListener {
                val notificationDAO =
                    MhealthRoomDatabase.getAppDataBase(activity)!!.notificationDAO()
                notificationDAO.deleteNotification(notification.notificationId)
                notificationList.removeAt(position)
                notifyDataSetChanged()
                if (notificationList.size <= 0)
                    notificationFragment.showNoItemLayout()
                
            }
            //setting on touch for delete
            itemView.rlNotiContainer.setOnTouchListener(object : View.OnTouchListener {
                override fun onTouch(v: View, event: MotionEvent): Boolean {
                    when (event.getAction()) {
                        MotionEvent.ACTION_DOWN -> {
                            downX = event.getX()
                            downY = event.getY()
                            return true
                        }
                        MotionEvent.ACTION_UP -> {
                            upX = event.getX()
                            upY = event.getY()
                            val deltaX = downX - upX
                            val deltaY = downY - upY
                            // swipe horizontal?
                            if (Math.abs(deltaX) > MIN_DISTANCE) {
                                // left or right
                                if (deltaX < 0) {
                                    //Toast.makeText(activity, "Left to Right", Toast.LENGTH_LONG).show()
                                    itemView.rlDel.visibility = View.GONE
                                    return true
                                }
                                if (deltaX > 0) {
                                    //Toast.makeText(activity, "Right to Left", Toast.LENGTH_LONG).show()
                                    itemView.rlDel.visibility = View.VISIBLE
                                    return true
                                }
                            } /*else {
                                Log.i(
                                    logTag,
                                    "Swipe was only " + Math.abs(deltaX) + " long horizontally, need at least " + MIN_DISTANCE
                                )
                                // return false; // We don't consume the event
                            }
                            // swipe vertical?
                            if (Math.abs(deltaY) > MIN_DISTANCE) {
                                // top or down
                                if (deltaY < 0) {
                                    // this.onTopToBottomSwipe()
                                    return true
                                }
                                if (deltaY > 0) {
                                    //this.onBottomToTopSwipe()
                                    return true
                                }
                            } else {
                                Log.i(
                                    logTag,
                                    "Swipe was only " + Math.abs(deltaX) + " long vertically, need at least " + MIN_DISTANCE
                                )
                                // return false; // We don't consume the event
                            }*/
                            return false // no swipe horizontally and no swipe vertically
                        }// case MotionEvent.ACTION_UP:
                    }
                    return false
                }
            })


        }
    }

    fun setNotificationList(notificationList: List<Notification>) {
        this.notificationList.clear()
        this.notificationList.addAll(notificationList)
        notifyDataSetChanged()
    }


}