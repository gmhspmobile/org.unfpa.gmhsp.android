package com.unfpa.mhealth.adapters.introductory

data class Type(val itemName: String, var isChecked: Boolean)
