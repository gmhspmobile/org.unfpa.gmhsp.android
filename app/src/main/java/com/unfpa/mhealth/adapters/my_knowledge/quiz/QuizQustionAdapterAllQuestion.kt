package com.unfpa.mhealth.adapters.my_knowledge.quiz

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.nostra13.universalimageloader.core.ImageLoader
import com.unfpa.mhealth.Mhealth
import com.unfpa.mhealth.R
import com.unfpa.mhealth.api_controller.EndPoints
import com.unfpa.mhealth.models.QuestionResponse
import com.unfpa.mhealth.utils.Constant
import org.json.JSONArray
import org.json.JSONObject


/**
 * Created by KhyatiShah on 10/12/2020.
 */
class QuizQustionAdapterAllQuestion(
    arrQuestionId: ArrayList<String>,
    arrAnswerId: ArrayList<String>
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var context: Context
    private lateinit var quizList: JSONArray
    val arrQuestionId = arrQuestionId
    val arrAnswerId = arrAnswerId
    private val lstResponse: MutableList<QuestionResponse> = mutableListOf<QuestionResponse>()

    companion object {
        const val TYPE_SINGLE_CHOICE_QUE = 0
        const val TYPE_MULTI_CHOICE_QUE = 1
        const val TYPE_IMAGE_BASED_QUE = 2
    }

    override fun getItemViewType(position: Int): Int {
        val questionJson = quizList.getJSONObject(position)
        val question_type = questionJson.getString("question_type")
        when (question_type) {
            Constant.QUE_TYPE_SINGLE_CHOICE -> return TYPE_SINGLE_CHOICE_QUE
            Constant.QUE_TYPE_MULTIPLE_CHOICE -> return TYPE_MULTI_CHOICE_QUE
            Constant.QUE_TYPE_IMAGE_SINGLE_CHOICE -> return TYPE_IMAGE_BASED_QUE
        }
        return TYPE_SINGLE_CHOICE_QUE
    }

    /* override fun onCreateViewHolder(
         parent: ViewGroup,
         viewType: Int
     ): QuizAllQuestionViewHolder {
         val v =
             LayoutInflater.from(parent.context)
                 .inflate(R.layout.item_quiz_all_questions, parent, false)
         context = parent.context
         return QuizAllQuestionViewHolder(
             v
         )
     }*/
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        when (viewType) {
            TYPE_IMAGE_BASED_QUE -> return ImageBasedViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_quiz_image_based, parent, false)
            )

            TYPE_MULTI_CHOICE_QUE -> return MultiChoiceViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_quiz_multi_choice, parent, false)
            )
            else -> return SingleChoiceViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_quiz_single_choice, parent, false)
            )
        }
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val questionJson = quizList.getJSONObject(position)
        //setting response
        val response = QuestionResponse(questionJson.get("id").toString(), false)
        lstResponse.add(response)

        if (holder is ImageBasedViewHolder) {
            bindImageBasedQue(holder, questionJson, position)
        } else if (holder is SingleChoiceViewHolder) {
            bindSingleChoiceQue(holder, questionJson, position)
        } else if (holder is MultiChoiceViewHolder) {
            bindMultiChoiceQue(holder, questionJson, position)
        }
    }

    fun bindImageBasedQue(holder: ImageBasedViewHolder, questionJson: JSONObject, position: Int) {
        //Question text
        holder.txtQuestion.text = "Q.   " + questionJson.get("title").toString()
        val answerJSON = questionJson.getJSONArray("answer_options")
        holder.ansOption1.setBackgroundResource(R.drawable.rect_blue_border_quiz)
        holder.ansOption2.setBackgroundResource(R.drawable.rect_blue_border_quiz)
        holder.ansOption3.setBackgroundResource(R.drawable.rect_blue_border_quiz)
        holder.ansOption4.setBackgroundResource(R.drawable.rect_blue_border_quiz)
        val imageLoader = ImageLoader.getInstance()
        var strURL: String
        var tempAns = ""
        for (i in 0 until answerJSON.length()) {
            strURL =
                (EndPoints.URL_ROOT + answerJSON.getJSONObject(i)
                    .getString("response_image_options")).replace("\\s".toRegex(), "")
            tempAns = answerJSON.getJSONObject(i).getString("response_text_options")
            when (i) {
                0 -> {
                    imageLoader.displayImage(strURL, holder.ansOption1, Mhealth.imageOptions)
                    holder.ansOption1.visibility = View.VISIBLE
                    if (tempAns.equals(arrAnswerId.get(position))) {
                        holder.ansOption1.setBackgroundResource(R.drawable.rect_green_border)
                    } else {
                        holder.ansOption1.setBackgroundResource(R.drawable.rect_blue_border_quiz)
                    }
                }
                1 -> {
                    imageLoader.displayImage(strURL, holder.ansOption2, Mhealth.imageOptions)
                    holder.ansOption2.visibility = View.VISIBLE
                    if (tempAns.equals(arrAnswerId.get(position))) {
                        holder.ansOption2.setBackgroundResource(R.drawable.rect_green_border)
                    } else {
                        holder.ansOption2.setBackgroundResource(R.drawable.rect_blue_border_quiz)
                    }
                }
                2 -> {
                    imageLoader.displayImage(strURL, holder.ansOption3, Mhealth.imageOptions)
                    holder.ansOption3.visibility = View.VISIBLE
                    if (tempAns.equals(arrAnswerId.get(position))) {
                        holder.ansOption3.setBackgroundResource(R.drawable.rect_green_border)
                    } else {
                        holder.ansOption3.setBackgroundResource(R.drawable.rect_blue_border_quiz)
                    }
                }
                3 -> {
                    imageLoader.displayImage(strURL, holder.ansOption4, Mhealth.imageOptions)
                    holder.ansOption4.visibility = View.VISIBLE
                    if (tempAns.equals(arrAnswerId.get(position))) {
                        holder.ansOption4.setBackgroundResource(R.drawable.rect_green_border)
                    } else {
                        holder.ansOption4.setBackgroundResource(R.drawable.rect_blue_border_quiz)
                    }
                }
            }
        }
        holder.ansOption1.setOnClickListener {
            /*arrAnswerId.removeAt(position)
            arrQuestionId.removeAt(position)
            arrAnswerId.add(
                position,
                answerJSON.getJSONObject(0).getString("response_text_options")
            )
            arrQuestionId.add(
                position,
                questionJson.getString("id")
            )*/
            holder.ansOption1.setBackgroundResource(R.drawable.rect_green_border)
            holder.ansOption2.setBackgroundResource(R.drawable.rect_blue_border_quiz)
            holder.ansOption3.setBackgroundResource(R.drawable.rect_blue_border_quiz)
            holder.ansOption4.setBackgroundResource(R.drawable.rect_blue_border_quiz)
            val idCorrectAns = questionJson.getString("correct_answer")
            val selectedAns = answerJSON.getJSONObject(0).getString("response_text_options")
            if (selectedAns.equals(idCorrectAns)) {
                lstResponse.get(position).isTrue = true
            } else {
                lstResponse.get(position).isTrue = false
            }
        }
        holder.ansOption2.setOnClickListener {
            /*arrAnswerId.removeAt(position)
            arrQuestionId.removeAt(position)
            arrAnswerId.add(
                position,
                answerJSON.getJSONObject(1).getString("response_text_options")
            )
            arrQuestionId.add(
                position,
                questionJson.getString("id")
            )*/
            holder.ansOption1.setBackgroundResource(R.drawable.rect_blue_border_quiz)
            holder.ansOption2.setBackgroundResource(R.drawable.rect_green_border)
            holder.ansOption3.setBackgroundResource(R.drawable.rect_blue_border_quiz)
            holder.ansOption4.setBackgroundResource(R.drawable.rect_blue_border_quiz)
            val idCorrectAns = questionJson.getString("correct_answer")
            val selectedAns = answerJSON.getJSONObject(1).getString("response_text_options")
            if (selectedAns.equals(idCorrectAns)) {
                lstResponse.get(position).isTrue = true
            } else {
                lstResponse.get(position).isTrue = false
            }
        }
        holder.ansOption3.setOnClickListener {
            /*arrAnswerId.removeAt(position)
            arrQuestionId.removeAt(position)
            arrAnswerId.add(
                position,
                answerJSON.getJSONObject(2).getString("response_text_options")
            )
            arrQuestionId.add(
                position,
                questionJson.getString("id")
            )*/
            holder.ansOption1.setBackgroundResource(R.drawable.rect_blue_border_quiz)
            holder.ansOption2.setBackgroundResource(R.drawable.rect_blue_border_quiz)
            holder.ansOption3.setBackgroundResource(R.drawable.rect_green_border)
            holder.ansOption4.setBackgroundResource(R.drawable.rect_blue_border_quiz)
            val idCorrectAns = questionJson.getString("correct_answer")
            val selectedAns = answerJSON.getJSONObject(2).getString("response_text_options")
            if (selectedAns.equals(idCorrectAns)) {
                lstResponse.get(position).isTrue = true
            } else {
                lstResponse.get(position).isTrue = false
            }
        }
        holder.ansOption4.setOnClickListener {
            /* arrAnswerId.removeAt(position)
             arrQuestionId.removeAt(position)

             arrAnswerId.add(
                 position,
                 answerJSON.getJSONObject(3).getString("response_text_options")
             )
             arrQuestionId.add(
                 position,
                 questionJson.getString("id")
             )*/
            holder.ansOption1.setBackgroundResource(R.drawable.rect_blue_border_quiz)
            holder.ansOption2.setBackgroundResource(R.drawable.rect_blue_border_quiz)
            holder.ansOption3.setBackgroundResource(R.drawable.rect_blue_border_quiz)
            holder.ansOption4.setBackgroundResource(R.drawable.rect_green_border)
            val idCorrectAns = questionJson.getString("correct_answer")
            val selectedAns = answerJSON.getJSONObject(3).getString("response_text_options")
            if (selectedAns.equals(idCorrectAns)) {
                lstResponse.get(position).isTrue = true
            } else {
                lstResponse.get(position).isTrue = false
            }
        }

    }

    fun bindSingleChoiceQue(
        holder: SingleChoiceViewHolder,
        questionJson: JSONObject,
        position: Int
    ) {
        //Question text
        holder.textViewName.text = questionJson.get("title").toString()
        //adding options
        val arrOptions = questionJson.getJSONArray("answer_options")
        for (i in 0 until arrOptions.length()) {
            val radioButton = RadioButton(context)
            val params =
                RadioGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            radioButton.setButtonDrawable(R.drawable.selector_radio_button_quiz)
            params.setMargins(0, 15, 0, 15)
            radioButton.setLayoutParams(params)
            radioButton.setPadding(15, 15, 0, 15)
            radioButton.compoundDrawablePadding = 15
            radioButton.tag = arrOptions.getJSONObject(i).getString("response_text_options")
            //radioButton.text = arrOptions.getJSONObject(i).getString("title")
            radioButton.text = arrOptions.getJSONObject(i).getString("response_text_options")
            holder.radioGroupOptions.addView(radioButton)
            holder.radioGroupOptions.setOnCheckedChangeListener { group, checkedId ->
                val selectedButton: RadioButton = group.findViewById(checkedId)
                val idCorrectAns = questionJson.getString("correct_answer")
                //lstResponse[position].isTrue = (selectedButton.tag) == idCorrectAns  //simplify version of below code
                if ((selectedButton.tag).equals(idCorrectAns)) {
                    lstResponse.get(position).isTrue = true
                } else {
                    lstResponse.get(position).isTrue = false
                }
                //Toast.makeText(context, lstResponse.get(position).isTrue.toString(), Toast.LENGTH_LONG).show()
            }
        }
    }

    fun bindMultiChoiceQue(
        holder: MultiChoiceViewHolder,
        questionJson: JSONObject,
        position: Int
    ) {
        //Question text
        holder.textViewName.text = questionJson.get("title").toString()

        //adding options
        val arrOptions = questionJson.getJSONArray("answer_options")
        for (i in 0 until arrOptions.length()) {
            val checkBox = CheckBox(context)
            val params =
                LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            // checkBox.setButtonDrawable(R.drawable.selector_radio_button_quiz)
            params.setMargins(0, 15, 0, 15)
            checkBox.setLayoutParams(params)
            checkBox.setPadding(15, 15, 0, 15)
            checkBox.compoundDrawablePadding = 15
            checkBox.tag = arrOptions.getJSONObject(i).getString("response_text_options")
            //checkBox.text = arrOptions.getJSONObject(i).getString("title")
            checkBox.text = arrOptions.getJSONObject(i).getString("response_text_options")
            holder.checkBoxOptions.addView(checkBox)
            checkBox.setOnClickListener(View.OnClickListener { v ->
                var ans: String = ""
                for (i in 0 until holder.checkBoxOptions.childCount) {
                    val v: View = holder.checkBoxOptions.getChildAt(i)
                    if (v is CheckBox && v.isChecked) {
                        ans = ans + v.tag + ","
                    }
                }
                if (ans.endsWith(",")) {
                    ans = ans.substring(0, ans.length - 1);
                }
                val idCorrectAns = questionJson.getString("correct_answer")
                if (ans.equals(idCorrectAns)) {
                    lstResponse.get(position).isTrue = true
                } else {
                    lstResponse.get(position).isTrue = false
                }
            })
        }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return quizList.length()
    }

    class ImageBasedViewHolder(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        val txtQuestion = itemView.findViewById(R.id.txtQuestion) as TextView
        val ansOption1 = itemView.findViewById(R.id.ansOption1) as ImageView
        val ansOption2 = itemView.findViewById(R.id.ansOption2) as ImageView
        val ansOption3 = itemView.findViewById(R.id.ansOption3) as ImageView
        val ansOption4 = itemView.findViewById(R.id.ansOption4) as ImageView
    }

    class SingleChoiceViewHolder(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        val textViewName = itemView.findViewById(R.id.txtQuestionName) as TextView
        val radioGroupOptions = itemView.findViewById(R.id.radioGroupOptions) as RadioGroup
    }

    class MultiChoiceViewHolder(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        val textViewName = itemView.findViewById(R.id.txtQuestionName) as TextView
        val checkBoxOptions = itemView.findViewById(R.id.checkBoxOptions) as LinearLayout
    }

    fun setQuizList(quizList: JSONArray) {
        this.quizList = quizList
        notifyDataSetChanged()
    }

    fun getResponse(): List<QuestionResponse> {
        return lstResponse
    }
}