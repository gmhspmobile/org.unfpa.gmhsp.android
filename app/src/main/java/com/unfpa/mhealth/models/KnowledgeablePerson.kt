package com.unfpa.mhealth.models

/**
 * Created by KhyatiShah on 12/6/2019.
 */
data class KnowledgeablePerson(
    var uId: String? = "",
    var name: String? = "",
    var countryCode: String? = ""
) {
}