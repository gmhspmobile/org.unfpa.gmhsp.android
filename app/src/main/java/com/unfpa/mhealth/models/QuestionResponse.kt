package com.unfpa.mhealth.models

/**
 * Created by KhyatiShah on 10/9/2019.
 */
data class QuestionResponse(
    var questionId: String?,
    var isTrue: Boolean
) {
}