package com.unfpa.mhealth.fragments.profile

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.unfpa.mhealth.R
import com.unfpa.mhealth.activities.profile.ProfileActivity
import com.unfpa.mhealth.adapters.introductory.IntroAdapter
import com.unfpa.mhealth.adapters.introductory.Type
import com.unfpa.mhealth.adapters.profile.IntroAdapterForInterest
import com.unfpa.mhealth.utils.Constant
import kotlinx.android.synthetic.main.fragment_slide_f_interested_fragement.*

class SlideFInterestedFragement : androidx.fragment.app.Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as AppCompatActivity).supportActionBar?.show()
        return inflater.inflate(R.layout.fragment_slide_f_interested_fragement, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        try {
            recyclerInterest.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this.context)
            val users = ArrayList<Type>()
            /*users.add(Type("Learn more about sexual and reproductive health topics.",false))
            users.add(Type("Get help fining a clinic.",false))
            users.add(Type("Talk to someone about my questions.",false))
            users.add(Type("Share my Story.",false))
            users.add(Type("Something else.",false))*/
            val adapter = IntroAdapterForInterest(
                (activity as ProfileActivity).getInterestList() as ArrayList<Type>,
                this.activity!!,
                Constant.ITEM_INTEREST
            )
            recyclerInterest.adapter = adapter
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
