package com.unfpa.mhealth.fragments.my_knowledge.quiz

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.unfpa.mhealth.R
import com.unfpa.mhealth.adapters.my_knowledge.quiz.QuizQustionAdapterAllQuestion
import com.unfpa.mhealth.api_controller.API_Controller
import com.unfpa.mhealth.api_controller.EndPoints
import com.unfpa.mhealth.api_controller.Listeners.Listener
import com.unfpa.mhealth.database.MhealthRoomDatabase
import com.unfpa.mhealth.database.entity.my_knowledge.QuizResponse
import com.unfpa.mhealth.utils.AppUtils
import com.unfpa.mhealth.utils.Constant
import kotlinx.android.synthetic.main.fragment_quiz_all_question.*
import kotlinx.android.synthetic.main.fragment_quiz_all_question.progressBar
import kotlinx.android.synthetic.main.popup_quiz_result.*
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by KhyatiShah on 10/12/2020.
 */
class QuizFragmentAllQuestion : androidx.fragment.app.Fragment(), Listener {

    lateinit var quizId: String
    lateinit var categoryId: String
    lateinit var quizName: String
    lateinit var adapter: QuizQustionAdapterAllQuestion
    lateinit var quizJSON: JSONObject
    lateinit var respnseQuiz: JSONObject
    lateinit var jsonQuestionList: JSONArray
    var scrollPos = 0
    var correctCount = 0
    var totalCount = 0
    var boolResult = 0
    lateinit var arrQuestionId: ArrayList<String>
    lateinit var arrAnswerId: ArrayList<String>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_quiz_all_question, container, false)
        try {
            setHasOptionsMenu(true)
            quizId = this.arguments!!.getString("questionId")
            categoryId = this.arguments!!.getString("categoryId")
            quizName = this.arguments!!.getString("quizName")
            quizJSON = JSONObject(this.arguments!!.getString("rawJSON"))

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try {
            //setting actionbar title
            (activity as AppCompatActivity).supportActionBar?.title = quizName

            setQuestion(view)
            setRecyclerScroll(
                lstQuestions,
                scrollPos
            )
            btnBack.setOnClickListener {
                setRecyclerScroll(
                    lstQuestions,
                    //linearLayoutManager.findLastVisibleItemPosition() - 1
                    scrollPos - 1
                )
                scrollPos--
            }
            btnNext.setOnClickListener {
                if (btnNext.text.toString().equals(getString(R.string.submit))) {
                    //scrollPos = 0
                    var isIncomplete = false
                    //check for all questions attempted
                    /*for (i in 0 until arrAnswerId.size) {
                        if (arrAnswerId.get(i).isNullOrEmpty()) {
                            isIncomplete = true
                            break
                        }
                    }
                    if (isIncomplete) {*/
                    /*val dialog = Dialog(activity)
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                    dialog.setCancelable(false)
                    dialog.setContentView(R.layout.dialog_one_buton)
                    dialog.show()
                    dialog.txtHeader.text = getString(R.string.QuizIncomplete)
                    dialog.txtMessage.text = getString(R.string.attend_questions)
                    dialog.txtOk.setOnClickListener {
                        dialog.dismiss()
                    }*/
                    //} else {
                    progressBar.visibility = View.VISIBLE
                    //Logging firebase screen
                    AppUtils.trackScreen(Constant.USER_QUIZ_RESPOND, activity!!)
                    submitQuiz()
                    //}
                } else {
                    setRecyclerScroll(
                        lstQuestions,
                        //linearLayoutManager.findLastVisibleItemPosition() + 1
                        scrollPos + 1
                    )
                    scrollPos++
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setQuestion(rootView: View) {
        try {
            val quizRequestDAO =
                MhealthRoomDatabase.getAppDataBase(activity!!.applicationContext)!!.quizRequestDAO()
            val quizRequest = quizRequestDAO.getQuizQuizIdWise(categoryId, quizId)
            val jsonQuiz = JSONObject(quizRequest.jsonQuiz)
            jsonQuestionList =
                jsonQuiz.getJSONArray("field_srh_quiz_question_export")

            lstQuestions.layoutManager = NoScrollLinearLayoutManager(context)
            (lstQuestions.layoutManager as NoScrollLinearLayoutManager).disableScrolling()
            arrQuestionId = ArrayList<String>(jsonQuestionList.length())
            arrAnswerId = ArrayList<String>(jsonQuestionList.length())
            for (i in 0 until jsonQuestionList.length()) {
                arrQuestionId.add("")
                arrAnswerId.add("")
            }
            adapter =
                QuizQustionAdapterAllQuestion(
                    arrQuestionId,
                    arrAnswerId
                )
            lstQuestions.adapter = adapter
            adapter.setQuizList(jsonQuestionList)
            adapter.notifyDataSetChanged()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setRecyclerScroll(
        lstQuestions: RecyclerView,
        toPos: Int
    ) {
        lstQuestions.scrollToPosition(toPos)
        txtQueCount.text = "Question No." + (toPos + 1) + "/" + jsonQuestionList.length()

        if (toPos < 1) {
            btnBack.isClickable = false
            btnBack.isEnabled = false
            btnBack.background.alpha = 80
        } else {
            btnBack.isClickable = true
            btnBack.isEnabled = true
            btnBack.background.alpha = 255
        }
        if (toPos >= jsonQuestionList.length() - 1) {
            /* btnNext.isClickable = false
             btnNext.background.alpha = 80*/
            btnNext.text = getString(R.string.submit)
        } else {
            btnNext.text = getString(R.string.next)
            btnNext.isClickable = true
            //btnNext.background.alpha = 255
        }
    }

    class NoScrollLinearLayoutManager(context: Context?) : LinearLayoutManager(
        context, RecyclerView.HORIZONTAL,
        false
    ) {
        private var scrollable = true

        fun enableScrolling() {
            scrollable = true
        }

        fun disableScrolling() {
            scrollable = false
        }

        override fun canScrollVertically() =
            super.canScrollVertically() && scrollable


        override fun canScrollHorizontally() =
            super.canScrollVertically()


    }

    fun submitQuiz() {
        try {
            correctCount = 0
            respnseQuiz = JSONObject()

            var correctAnsId = JSONArray()
            val listResponse = adapter.getResponse()
            for (i in 0 until listResponse.size) {
                if (listResponse.get(i).isTrue) {
                    correctCount += 1
                    correctAnsId.put(listResponse.get(i).questionId)
                }
            }
            //passing criteria
            val passingPer = quizJSON.getString("field_passing_criteria").toInt()
            totalCount = quizJSON.getJSONArray("field_srh_quiz_question_export").length()
            val marksSecured = (correctCount * 100) / totalCount

            if (passingPer <= marksSecured) {
                boolResult = 1
            }
            val sharedPreference =
                activity!!.getSharedPreferences(Constant.PREF_NAME, Context.MODE_PRIVATE)
            //activity!!.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
            respnseQuiz.put("title", JSONObject().put("value", quizName))
            respnseQuiz.put("field_srh_quiz", JSONObject().put("value", quizId))
            respnseQuiz.put(
                "field_respondent_age_group",
                JSONObject().put("value", sharedPreference.getString(Constant.PREF_AGE_GROUP, ""))
            )
            respnseQuiz.put(
                "field_respondent_country",
                JSONObject().put("value", sharedPreference.getString(Constant.PREF_COUNTRY, ""))
            )
            /*respnseQuiz.put(
                "field_respondent_unique_id",
                JSONObject().put("value", "6b9cb3b7-3519-4fe1-acf7-69183581270c")
            )*/
            respnseQuiz.put(
                "field_respondent_unique_id",
                JSONObject().put("value", AppUtils.getUUID(activity?.applicationContext!!))
            )
            respnseQuiz.put(
                "field_respondent_gender",
                JSONObject().put("value", sharedPreference.getString(Constant.PREF_GENDER, ""))
            )
            respnseQuiz.put(
                "field_quiz_status",
                JSONObject().put("value", boolResult).put("format", "boolean")
            )
            respnseQuiz.put("field_srh_quiz_question", JSONObject().put("value", correctAnsId))
            //API call
            API_Controller.postWithToken(
                context!!.applicationContext,
                respnseQuiz,
                EndPoints.URL_POST_QUIZ,
                this
            )

        } catch (e: Exception) {
            progressBar.visibility = View.GONE
            e.printStackTrace()
        }
    }

    override fun onComplete(response: String) {

        val quizResponseDAO = MhealthRoomDatabase.getAppDataBase(context!!)!!.quizResponseDAO()
        if (response != null || !TextUtils.isEmpty(response)) {
            quizResponseDAO.insert(
                QuizResponse(
                    categoryId,
                    quizId,
                    respnseQuiz.toString(),
                    1,
                    boolResult
                )
            )
        } else {
            quizResponseDAO.insert(
                QuizResponse(
                    categoryId,
                    quizId,
                    respnseQuiz.toString(),
                    0,
                    boolResult
                )
            )
        }
        progressBar.visibility = View.GONE
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.popup_quiz_result)
        dialog.txtScore.text = correctCount.toString() + " / " + totalCount.toString()
        if (boolResult == 1) {
            dialog.txtFaliled.visibility = View.INVISIBLE
            dialog.txtPassed.visibility = View.VISIBLE
            dialog.btnTryAgain.visibility = View.GONE
            dialog.imgQuizResult.setImageResource(R.drawable.ic_quiz_passed_big)
        } else {
            dialog.txtFaliled.visibility = View.VISIBLE
            dialog.txtPassed.visibility = View.INVISIBLE
            dialog.btnTryAgain.visibility = View.VISIBLE
            dialog.imgQuizResult.setImageResource(R.drawable.ic_quiz_failed_big)
        }
        dialog.txtClose.setOnClickListener {
            dialog.dismiss()
            getActivity()?.onBackPressed();
        }
        dialog.btnTryAgain.setOnClickListener {
            dialog.dismiss()

            val ft: FragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentManager!!.popBackStack()
            ft.commit()
            //Logging firebase screen
            AppUtils.trackScreen(Constant.USER_QUIZ_TAKE, activity!!)
            var bundle = Bundle()
            bundle.putString("questionId", quizId)
            bundle.putString("categoryId", categoryId)
            bundle.putString("quizName", quizName)
            bundle.putString("rawJSON", quizJSON.toString())
            //var frag = QuizFragmentOld()
            var frag = QuizFragmentAllQuestion()
            frag.arguments = bundle
            AppUtils.addFragment(activity!!, frag, true, "")
        }
        dialog.show()
    }
}