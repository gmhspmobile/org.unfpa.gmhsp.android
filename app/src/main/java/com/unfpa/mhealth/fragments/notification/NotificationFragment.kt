package com.unfpa.mhealth.fragments.notification

import android.graphics.*
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import com.unfpa.mhealth.R
import com.unfpa.mhealth.adapters.my_knowledge.SRHContentListAdapter
import com.unfpa.mhealth.adapters.notification.NotificationAdapter
import com.unfpa.mhealth.database.MhealthRoomDatabase
import com.unfpa.mhealth.database.entity.my_knowledge.SRHContent
import com.unfpa.mhealth.database.entity.notification.Notification
import kotlinx.android.synthetic.main.fragment_notification.*
import androidx.recyclerview.widget.ItemTouchHelper.SimpleCallback
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar


/**
 * Created by KhyatiShah on 12/13/2019.
 */
class NotificationFragment : androidx.fragment.app.Fragment() {

    private val p = Paint()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.fragment_notification, container, false)
        setHasOptionsMenu(true)

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try {
            //setting actionbar title
            (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.Notifications)
            lstNotification.layoutManager =
                androidx.recyclerview.widget.LinearLayoutManager(this.context) as androidx.recyclerview.widget.RecyclerView.LayoutManager?
            val notificationAdapter = NotificationAdapter(this)
            lstNotification.adapter = notificationAdapter
            val notificationDAO =
                MhealthRoomDatabase.getAppDataBase(this.activity!!.applicationContext)!!.notificationDAO()
            val listNotification: List<Notification> = notificationDAO?.getAllNotification()
            notificationAdapter.setNotificationList(listNotification)
            if (listNotification.size <= 0) {
                linearLayoutNoArticle.visibility = View.VISIBLE
                lstNotification.visibility = View.GONE
            } else {
                linearLayoutNoArticle.visibility = View.GONE
                lstNotification.visibility = View.VISIBLE
            }
            //val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
            // itemTouchHelper.attachToRecyclerView(lstNotification)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        if (menu != null) {
            menu!!.findItem(R.id.notification).isVisible = false
            menu!!.findItem(R.id.home).isVisible = true
        }
    }

    fun showNoItemLayout() {
        linearLayoutNoArticle.visibility = View.VISIBLE
        lstNotification.visibility = View.GONE
    }
    /*var simpleItemTouchCallback: ItemTouchHelper.SimpleCallback = object : ItemTouchHelper.SimpleCallback(
        0,
        ItemTouchHelper.LEFT
        //or ItemTouchHelper.RIGHT or ItemTouchHelper.DOWN or ItemTouchHelper.UP
    ) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            // Toast.makeText(activity, "on Move", Toast.LENGTH_SHORT).show()
            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            Toast.makeText(activity, "on Swiped ", Toast.LENGTH_SHORT).show()
            val position = viewHolder.adapterPosition

            if (direction == ItemTouchHelper.LEFT) {
                *//* val deletedModel = imageModelArrayList!![position]
                 adapter!!.removeItem(position)*//*
                // showing snack bar with Undo option
                val snackbar = Snackbar.make(
                    this@NotificationFragment!!.activity!!.window.decorView.rootView,
                    " removed from Recyclerview!",
                    Snackbar.LENGTH_LONG
                )
                snackbar.setAction("UNDO") {
                    // undo is selected, restore the deleted item
                    // adapter!!.restoreItem(deletedModel, position)
                }
                snackbar.setActionTextColor(Color.YELLOW)
                snackbar.show()
            } else {
                *//*val deletedModel = imageModelArrayList!![position]
                adapter!!.removeItem(position)*//*
                // showing snack bar with Undo option
                val snackbar = Snackbar.make(
                    this@NotificationFragment!!.activity!!.window.decorView.rootView,
                    " removed from Recyclerview!",
                    Snackbar.LENGTH_LONG
                )
                snackbar.setAction("UNDO") {
                    // undo is selected, restore the deleted item
                    // adapter!!.restoreItem(deletedModel, position)
                }
                snackbar.setActionTextColor(Color.YELLOW)
                snackbar.show()
            }
        }

        override fun onChildDraw(
            c: Canvas,
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            dX: Float,
            dY: Float,
            actionState: Int,
            isCurrentlyActive: Boolean
        ) {

            val icon: Bitmap
            if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                val itemView = viewHolder.itemView
                val height = itemView.bottom.toFloat() - itemView.top.toFloat()
                val width = height / 3

                if (dX > 0) {
                    p.color = ContextCompat.getColor(this@NotificationFragment!!.activity!!, R.color.white);
                    val background =
                        RectF(itemView.left.toFloat(), itemView.top.toFloat(), dX, itemView.bottom.toFloat())
                    c.drawRect(background, p)
                    icon = BitmapFactory.decodeResource(resources, R.drawable.ic_del)
                    val icon_dest = RectF(
                        itemView.left.toFloat() + width,
                        itemView.top.toFloat() + width,
                        itemView.left.toFloat() + 2 * width,
                        itemView.bottom.toFloat() - width
                    )
                    c.drawBitmap(icon, null, icon_dest, p)
                } else {
                    p.color = ContextCompat.getColor(this@NotificationFragment!!.activity!!, R.color.white);
                    val background = RectF(
                        itemView.right.toFloat() + dX,
                        itemView.top.toFloat(),
                        itemView.right.toFloat(),
                        itemView.bottom.toFloat()
                    )
                    c.drawRect(background, p)
                    icon = BitmapFactory.decodeResource(resources, R.drawable.ic_del)
                    val icon_dest = RectF(
                        itemView.right.toFloat() - 2 * width,
                        itemView.top.toFloat() + width,
                        itemView.right.toFloat() - width,
                        itemView.bottom.toFloat() - width
                    )
                    c.drawBitmap(icon, null, icon_dest, p)
                }
            }
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        }


    }
*/

}