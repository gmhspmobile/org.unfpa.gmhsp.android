package com.unfpa.mhealth.activities

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.unfpa.mhealth.Mhealth
import com.unfpa.mhealth.R
import com.unfpa.mhealth.api_controller.API_Controller
import com.unfpa.mhealth.database.MhealthRoomDatabase
import com.unfpa.mhealth.utils.AppUtils
import kotlinx.android.synthetic.main.activity_splash.*
import com.nostra13.universalimageloader.core.ImageLoader
import com.unfpa.mhealth.api_controller.EndPoints
import com.unfpa.mhealth.utils.Constant


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class SplashActivity_old : AppCompatActivity() {

    //lateinit var progressBar: ProgressBar
    lateinit var mainHandler: Handler
    private var fieldImage: String = ""
    private val updateTextTask = object : Runnable {
        override fun run() {
            checkInternet()
            mainHandler.postDelayed(this, 10000)
        }
    }

    override fun onPause() {
        super.onPause()
        mainHandler.removeCallbacks(updateTextTask)

    }

    override fun onResume() {
        super.onResume()
        mainHandler.post(updateTextTask)
        //Logging firebase screen
        AppUtils.trackScreen(Constant.APP_OPEN, this)
    }

    fun checkInternet() {

        //cd.isConnectingToInternet(this@SplashActivity)
        val sharedPreference = getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        val allowedInternet = sharedPreference.getBoolean("allowedInternet", false)
        var editor = sharedPreference.edit()
        val countryOfficeDAO =
            MhealthRoomDatabase.getAppDataBase(this.applicationContext)!!.countryOfficeDAO()
                .getAllContent()
        if (countryOfficeDAO.isNotEmpty()) {
            val CountryCode = sharedPreference.getString("CountryCode", "")
            val countryOfficeDAO_1 =
                MhealthRoomDatabase.getAppDataBase(applicationContext)!!.countryOfficeDAO()
            val countryCodeRequest = countryOfficeDAO_1.getModuleVisibility(CountryCode)
            if (CountryCode != "") {
                fieldImage = countryCodeRequest.field_image
            }
            if (fieldImage.isNotEmpty()) {
                val imageLoader = ImageLoader.getInstance()
                val strURL = (EndPoints.URL_ROOT + fieldImage).replace("\\s".toRegex(), "")
                imageLoader.displayImage(strURL, imgSplash, Mhealth.imageOptions)
            }
        }
        if (AppUtils.isConnectingToInternet(this)) {
            if (allowedInternet) {
                Toast.makeText(
                    applicationContext,
                    getString(R.string.you_are_connected_to_internet),
                    Toast.LENGTH_LONG
                )
                    .show()
                editor.putBoolean("allowedInternet", false)
                editor.apply()
                editor.commit()
            }

            FetchDataTask().execute()
        } else {
            if (countryOfficeDAO.isEmpty()) {
                Toast.makeText(
                    applicationContext,
                    getString(R.string.please_connect_to_internet),
                    Toast.LENGTH_LONG
                )
                    .show()
                editor.putBoolean("allowedInternet", true)
                editor.apply()
                editor.commit()
            } else {

                FetchDataTask().execute()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        try {
            //progressBar = findViewById(R.id.progressBar)
            mainHandler = Handler(Looper.getMainLooper())
            val sharedPreference =
                this.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
            var editor = sharedPreference.edit()
            editor.putBoolean("flag1", false)
            editor.apply()
            editor.commit()
            //AppUtils.getAddress(20.7702, 72.9824, this)
            //val strCount = AppUtils.getCountryCode(20.6903, 72.9497, this)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    inner class FetchDataTask() : AsyncTask<Void, Void, String>() {
        override fun doInBackground(vararg params: Void?): String? {

            Thread.sleep(5500)
            val countryOfficeDAO =
                MhealthRoomDatabase.getAppDataBase(applicationContext)!!.countryOfficeDAO()
                    .getAllContent()
            if (countryOfficeDAO.isEmpty()) {
                /* API_Controller.getCountryOfficeList(applicationContext)//added by 35251
                 API_Controller.getContentCategories(applicationContext)
                 API_Controller.getContentList(applicationContext)
                 API_Controller.getServiceCenterDetail(applicationContext) //added by 35251
                 API_Controller.getQuiz(applicationContext)*/
                //API_Controller.getToken(applicationContext)

            } else {
                AppUtils.isConnectingToInternet(this@SplashActivity_old)
                if (AppUtils.isConnectingToInternet(this@SplashActivity_old)) {
                    /* API_Controller.getCountryOfficeList(applicationContext,this)//added by 35251
                     API_Controller.getContentCategories(applicationContext)
                     API_Controller.getContentList(applicationContext)
                     API_Controller.getServiceCenterDetail(applicationContext) //added by 35251
                     API_Controller.getQuiz(applicationContext)*/
                }
            }
            //return API_Controller.getMyServiceList(applicationContext) //added by 35251
            return ""
        }

        override fun onPreExecute() {
            super.onPreExecute()
            progressBar.visibility = View.VISIBLE
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            progressBar.visibility = View.INVISIBLE
            val sharedPreference = getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
            val deniedLocationPermission = sharedPreference.getBoolean("IntroFlag", false)
            intent = if (!deniedLocationPermission) {
                Intent(applicationContext, IntroductoryActivity::class.java)
            } else {
                Intent(applicationContext, MainActivity::class.java)
            }
            startActivity(intent)
            finish()
        }

    }
}


