package com.unfpa.mhealth.database.entity.personaldetails

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

//added by 35251

@Entity(tableName = "personal_details")
data class PersonalDetailsEntity(
    @PrimaryKey var unique_id: String,
    var title: String
) {
}