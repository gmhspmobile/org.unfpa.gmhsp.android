package com.unfpa.mhealth.database.entity.my_knowledge

import androidx.room.Entity
import androidx.room.PrimaryKey
import io.reactivex.annotations.NonNull

/**
 * Created by KhyatiShah on 9/25/2019.
 */
@Entity(tableName = "quiz_request", primaryKeys = arrayOf("categoryId", "quizId"))
data class QuizRequest(
    var categoryId: String,
    var quizId: String,
    var jsonQuiz: String?
) {
}