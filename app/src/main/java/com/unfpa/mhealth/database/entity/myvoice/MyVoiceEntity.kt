package com.unfpa.mhealth.database.entity.myvoice

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

//added by 35251

@Entity(tableName = "my_voice")
data class MyVoiceEntity(
    @PrimaryKey var unique_id: String,
    var title: String,
    var story: String,
    var story_mode: String
) {
}