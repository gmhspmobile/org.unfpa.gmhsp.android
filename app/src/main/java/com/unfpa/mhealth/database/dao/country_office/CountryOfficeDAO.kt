package com.unfpa.mhealth.database.dao.personal

import androidx.room.*
import com.unfpa.mhealth.database.entity.country_office_listing.CountryOfficeSettingEntity
import com.unfpa.mhealth.database.entity.myvoice.MyVoiceEntity
import com.unfpa.mhealth.database.entity.personaldetails.PersonalDetailsEntity

//added by 35251

@Dao
interface CountryOfficeDAO {
    @Query("SELECT * from country_office_listing")
    fun getAllContent(): List<CountryOfficeSettingEntity>


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(countryOfficeSettingEntity: CountryOfficeSettingEntity)

    @Query("SELECT * from country_office_listing WHERE field_country = :CountryCode")
    fun getModuleVisibility(CountryCode: String): CountryOfficeSettingEntity

    @Query("DELETE FROM country_office_listing")
    fun deleteAll()
}