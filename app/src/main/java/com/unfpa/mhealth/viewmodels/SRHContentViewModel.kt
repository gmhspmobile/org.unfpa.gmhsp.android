package com.unfpa.mhealth.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import android.content.Context
import com.unfpa.mhealth.database.MhealthRoomDatabase
import com.unfpa.mhealth.database.entity.my_knowledge.SRHContent

class SRHContentViewModel(context: Context) : ViewModel() {

    private val listLiveData: List<SRHContent>

    init {
        val mHealthRoomDB = MhealthRoomDatabase.getAppDataBase(context)
        val contentMasterDAO = mHealthRoomDB?.contentMasterDAO()!!
        listLiveData = contentMasterDAO?.getAllContent()
    }

    fun getSRHContentMaster(): List<SRHContent> {
        return listLiveData
    }

}